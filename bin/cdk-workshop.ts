#!/usr/bin/env node
import * as cdk from "aws-cdk-lib";
import { GuillaumeCdkPlayground } from "../lib/cdk-workshop-stack";

const app = new cdk.App();
new GuillaumeCdkPlayground(app, "GuillaumeCdkPlayground");
